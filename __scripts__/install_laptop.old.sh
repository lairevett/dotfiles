#!/usr/bin/env sh
# -*- coding: utf-8 -*-

DOTS_PATH=$HOME/Documents/dev/repos/dotfiles

echo "Making directories..."
mkdir $HOME/.config/sway

[[ -d $HOME/.local/bin ]] || mkdir $HOME/.local/bin
[[ -d $HOME/.local/lib/python3_contrib ]] || mkdir -p $HOME/.local/lib/python3_contrib
mkdir $HOME/.config/i3status

mkdir $HOME/.config/termite
mkdir $HOME/.config/dunst
mkdir $HOME/.config/qt5ct
mkdir $HOME/.config/oomox
mkdir -p $HOME/.config/rofi/themes
mkdir /usr/share/backgrounds/$USER
chown $USER:users /usr/share/backgrounds/$USER
mkdir $HOME/.config/gtk-2.0
mkdir $HOME/.config/gtk-3.0

mkdir $HOME/.vim

echo "Linking dotfiles..."
ln -s $DOTS_PATH/sway/config $HOME/.config/sway/

ln -s $DOTS_PATH/i3status/bin/i3status_injector.py $HOME/.local/bin/
ln -s $DOTS_PATH/i3status/lib/i3status_module_*.py $HOME/.local/lib/python3_contrib/
ln -s $DOTS_PATH/i3status/config/top_laptop.conf $HOME/.config/i3status/top.conf
ln -s $DOTS_PATH/i3status/config/bottom_laptop.conf $HOME/.config/i3status/bottom.conf

ln -s $DOTS_PATH/termite/config $HOME/.config/termite/
ln -s $DOTS_PATH/dunst/dunstrc $HOME/.config/dunst/
ln -s $DOTS_PATH/qt5ct/qt5ct.conf $HOME/.config/qt5ct/
ln -s $DOTS_PATH/qt5ct/colors $HOME/.config/qt5ct/
ln -s $DOTS_PATH/oomox/colors/ $HOME/.config/oomox/colors/
ln -s $DOTS_PATH/rofi/lairevett.rasi $HOME/.config/rofi/themes/
ln -s $DOTS_PATH/wallpaper.jpg /usr/share/backgrounds/$USER/
ln -s $DOTS_PATH/zsh/zhsrc_laptop /etc/zsh/
ln -s $DOTS_PATH/zsh/zshrc_laptop $HOME/.zshrc
ln -s $DOTS_PATH/zsh/lairevett.zsh-theme /opt/oh-my-zsh/themes/
ln -s $DOTS_PATH/urxvt/Xdefaults $HOME/.Xdefaults
ln -s $DOTS_PATH/gtk/gtkfilechooser.ini $HOME/.config/gtk-2.0/
ln -s $DOTS_PATH/gtk/gtk-3.0.ini $HOME/.config/gtk-3.0/settings.ini
ln -s $DOTS_PATH/gtk/gtkrc-2.0 $HOME/.gtkrc-2.0

ln -s $DOTS_PATH/npm/npmrc $HOME/.npmrc

ln -s $DOTS_PATH/linters/eslintrc-vanilla.js $HOME/.eslintrc.js
ln -s $DOTS_PATH/linters/pylintrc $HOME/.pylintrc

ln -s $DOTS_PATH/formatters/prettierrc.js $HOME/.prettierrc.js
ln -s $DOTS_PATH/formatters/pep8 $HOME/.config/

ln -s $DOTS_PATH/vim/vimrc $HOME/.vimrc
ln -s $DOTS_PATH/vim/misc $HOME/.vim/
ln -s $DOTS_PATH/vim/plugins.vim $HOME/.vim/

echo "Don't forget to install vim-plug, instant-markdown-d and htmlhint!"
echo ":PlugInstall, :CocInstall coc-spell-checker, coc-cspell-dicts, coc-highlight, coc-pairs, coc-yank, coc-git, coc-prettier, coc-eslint, coc-jest, coc-tsserver, coc-emmet, coc-html, coc-svg, coc-css, coc-python, coc-phpls, coc-json, coc-yaml, coc-xml, coc-sh, coc-markdownlint"

