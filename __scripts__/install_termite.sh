#!/bin/sh

PGK_CONFIG_PATH=/usr/local/lib/pkgconfig

git clone --recursive https://github.com/thestinger/vte-ng.git
git clone --recursive https://github.com/lairevett/termite.git

sudo pkg install gmake pkgconf gtk-doc autoconf gettext autotools intltool gperf

cd vte-ng
./autogen.sh --disable-vala --disable-dependency-tracking
PKG_CONFIG_PATH=$PKG_CONFIG_PATH gmake
sudo PKG_CONFIG_PATH=$PKG_CONFIG_PATH gmake install

sudo pkg install ncurses

cd ../termite
PKG_CONFIG_PATH=$PKG_CONFIG_PATH gmake
sudo mkdir -p /etc/xdg/termite
sudo mkdir /usr/local/share/man/man5
sudo PKG_CONFIG_PATH=$PKG_CONFIG_PATH gmake install
