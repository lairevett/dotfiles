repo_dir="~/Documents/repos/dotfiles"

echo "Make new XDG directories"
mkdir ~/.local/bin
mkdir ~/.local/etc
mkdir ~/.local/lib
mkdir -p ~/.local/var/cache
mkdir ~/.local/var/run
mkdir ~/.local/share

echo "Link binaries"
ln -s "$repo_dir"/bin/* ~/.local/bin/
rm -rf ~/.local/bin/__freebsd_bin__

echo "Link libraries"
ln -s "$repo_dir"/lib/python3_modules ~/.local/lib/

echo "Link etc"
[ ! -d "~/.local/etc/coc" ] && mkdir ~/.local/etc/coc
ln -s "$repo_dir"/etc/coc/coc-settings.json ~/.local/etc/coc/
ln -s "$repo_dir"/etc/dunst ~/.local/etc/
ln -s "$repo_dir"/etc/gtk-2.0 ~/.local/etc/
ln -s "$repo_dir"/etc/gtk-3.0 ~/.local/etc/
ln -s "$repo_dir"/etc/i3 ~/.local/etc/
ln -s "$repo_dir"/etc/i3status ~/.local/etc/
ln -s "$repo_dir"/etc/js/eslintrc-vanilla.js ~/.eslintrc.js
mkdir ~/.local/etc/npm
ln -s "$repo_dir"/etc/npm/npmrc_freebsd ~/.local/etc/npm/
ln -s "$repo_dir"/etc/prettierrc.js ~/.prettierrc.js
ln -s "$repo_dir"/etc/python ~/.local/etc/
ln -s "$repo_dir"/etc/qt5ct ~/.local/etc/
ln -s "$repo_dir"/etc/termite ~/.local/etc/
mkdir ~/.local/etc/X11
ln -s "$repo_dir"/etc/X11/xinitrc ~/.local/etc/X11/xinitrc
ln -s "$repo_dir"/etc/X11/xserverrc ~/.local/etc/X11/xserverrc
ln -s "$repo_dir"/etc/X11/Xresources ~/.local/etc/X11/
sudo ln -s "$repo_dir"/etc/X11/xorg.conf /usr/local/etc/X11/
mkdir ~/.local/etc/zsh
mkdir ~/.local/share/zsh
ln -s "$repo_dir"/etc/zsh/zshrc_freebsd ~/.local/etc/zsh/.zshrc
ln -s "$repo_dir"/etc/zsh/zprofile_freebsd ~/.local/etc/zsh/.zprofile
ln -s "$repo_dir"/etc/git ~/.local/etc/

echo "Link data"
[ ! -d "~/.local/share/applications" ] && mkdir ~/.local/share/applications
ln -s "$repo_dir"/usr/share/applications/* ~/.local/share/applications/
ln -s "$repo_dir"/usr/share/rofi ~/.local/share/
mkdir ~/.local/share/vim-shared
ln -s "$repo_dir"/usr/share/vim-shared/* ~/.local/share/vim-shared/
mkdir ~/.local/share/{themes,icons}
tar -xvf "$repo_dir"/usr/share/themes/oomox-black.tar
tar -xvf "$repo_dir"/usr/share/icons/oomox-black.tar
mv "$repo_dir"/usr/share/themes/oomox-black ~/.local/share/themes/
mv "$repo_dir"/usr/share/icons/oomox-black ~/.local/share/icons/

echo "Make vim data directories"
mkdir -p ~/.local/share/vim/{after,backup,swap,undo,view}

echo "Apply login_conf"
mv "$repo_dir"/login_conf ~/.login_conf
cap_mkdb ~/.login_conf

echo "Configure ssh"
yes | sudo pkg install lsof
mkdir ~/.local/etc/ssh/
cp "$repo_dir"/etc/ssh/config ~/.local/etc/ssh/config
mkdir -p ~/.local/share/ssh/keys
mkdir -p ~/.local/var/run/ssh

echo "Configure var"
mkdir -p ~/.local/var/run/X11
mkdir -p ~/.local/var/cache/python

echo "Install vim-plug"
curl -fLo ~/.local/share/vim-shared/autoload/plug.vim \
    --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# echo "Install pyenv"
# curl pyenv.run | zsh

echo "Additional npm pkgs - eslint, eslint-config-prettier, prettier, instant-markdown-d, htmlhint, jest, @bitwarden/cli wscat"
echo "Additional vim commands - :PlugInstall, :CocInstall coc-spell-checker coc-cspell-dicts coc-highlight coc-yank coc-git coc-prettier coc-eslint coc-jest coc-tsserver coc-emmet coc-html coc-svg coc-css coc-python coc-phpls coc-json coc-yaml coc-xml coc-sh coc-markdownlint"

