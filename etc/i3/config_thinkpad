font pango:Blex Mono Nerd Font 10
focus_follows_mouse no

# Variables.
set $refresh_i3status killall -SIGUSR1 i3status
set $mod Mod4
set $alt Mod1

set $mode_system SYS
set $mode_wm WM
set $mode_resize RES

set $left h
set $down j
set $up k
set $right l

set $ws1 "1:  "
set $ws2 "2:  "
set $ws3 "3:  "
set $ws4 "4:  "
set $ws5 "5:  "
set $ws6 "6:  "
set $ws7 "7:  "
set $ws8 "8:  "

set $term alacritty
set $menu "rofi -modi drun -show drun -theme ~/.local/share/rofi/themes/lairevett"

set $gnome_iface "org.gnome.desktop.interface"
set $gnome_wm "org.gnome.desktop.wm.preferences"
set $gtk_theme "oomox-black"
set $icon_theme "oomox-black"
set $cursor_theme "Adwaita"

set $wallpaper_path "/usr/home/lairevett/Pictures/wallpaper.png"
set $lock i3lock -n

set $scrot_dir "$(xdg-user-dir PICTURES)/screenshots"
set $scrot_file "%F_%T_\$wx\$h_scrot.png"
set $scrot_xclip "xclip -selection clipboard -target image/png -i \$f"

# Key bindings.
bindsym $mod+Return exec $term
bindsym $mod+Shift+q kill
bindsym $mod+d exec $menu
floating_modifier $mod

bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8

# Mod + comma.
bindcode $mod+59 workspace prev
# Mod + dot.
bindcode $mod+60 workspace next

bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8

bindsym $mod+b split h
bindsym $mod+v split v

bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

bindsym $mod+f fullscreen
bindsym $mod+Shift+space floating toggle

bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

mode "$mode_system" {
    bindsym s exec sudo suspend
    bindsym h exec sudo hibernate
    bindsym r exec sudo reboot
    bindsym p exec sudo poweroff

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+Shift+s mode "$mode_system"

mode "$mode_wm" {
    bindsym l exec $lock -i $wallpaper_path, mode "default"
    bindsym c reload
    bindsym r restart

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+Shift+w mode "$mode_wm"

mode "$mode_resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+Shift+r mode "$mode_resize"

set $turn_down_volume exec --no-startup-id mixer vol -5
set $turn_up_volume exec --no-startup-id mixer vol +5

bindsym XF86AudioLowerVolume $turn_down_volume && $refresh_i3status
bindsym XF86AudioRaiseVolume $turn_up_volume && $refresh_i3status

bindsym --release Print exec --no-startup-id scrot $scrot_dir/$scrot_file
bindsym --release Control+Print exec --no-startup-id scrot /tmp/$scrot_file -e $scrot_xclip
bindsym --release Shift+Print exec --no-startup-id scrot -s $scrot_dir/$scrot_file
bindsym --release Control+Shift+Print exec --no-startup-id scrot -s /tmp/$scrot_file -e $scrot_xclip

# Client colors.
#
# class                 border  backg.  text    indicator child_border
client.focused          #000000 #000000 #ffffff #000000   #000000
client.focused_inactive #000000 #000000 #ffffff #000000   #000000
client.unfocused        #000000 #000000 #808080 #000000   #000000
client.urgent           #000000 #000000 #ff0000 #000000   #000000
client.placeholder      #000000 #000000 #ffffff #000000   #000000
client.background       #000000

# Status bars.
bar {
    position top
    status_command i3status --config ~/.local/etc/i3status/top_thinkpad.conf | i3status_injector.py wlan_quality_and_frequency battery_remaining user
    separator_symbol "  "
    strip_workspace_numbers yes
    binding_mode_indicator no
    colors {
        background #000000
        statusline #ffffff
        inactive_workspace #000000 #000000 #808080
        active_workspace #000000 #000000 #ffffff
        focused_workspace #000000 #000000 #ffffff
        urgent_workspace #000000 #000000 #ff0000
    }
}

bar {
    position bottom
    status_command i3status --config ~/.local/etc/i3status/bottom_freebsd.conf | i3status_injector.py os merge_cpu_usage_with_temp ram_usage_freebsd net_throughput_freebsd uptime_freebsd
    separator_symbol " | "
    tray_output none
    workspace_buttons no
    colors {
        background #000000
        statusline #ffffff
        separator #ffffff
    }
}

# App-to-workspace bindings.
assign [class="^Code$"] $ws3
assign [class="^jetbrains-webstorm$"] $ws3
assign [class="^jetbrains-pycharm$"] $ws3
assign [class="^Chromium-browser$" title="open.spotify.com"] $ws4
assign [class="^mpv$"] $ws4
assign [class="^vlc$"] $ws4
assign [class="^Chromium-browser$" title="messages.google.com"] $ws6
assign [class="^Chromium-browser$" title="web.whatsapp.com"] $ws6
assign [class="^Chromium-browser$" title="web.skype.com"] $ws6
assign [class="^Chromium-browser$" title="discord.com"] $ws6

# Custom WM workspace/app settings.
for_window [workspace=$ws6] layout stacking
for_window [workspace=$ws7] layout stacking
for_window [class="^Firefox$" title="^Developer Tools"] floating enable
for_window [class="^Firefox$" title="^Library$"] floating enable
for_window [class="^Chromium-browser$" title="^DevTools"] floating enable
for_window [class="^scrcpy$"] floating enable

# Startup commands.
exec --no-startup-id mixer vol 0
exec_always --no-startup-id gsettings set $gnome_iface gtk-theme $gtk_theme
exec_always --no-startup-id gsettings set $gnome_wm theme $gtk_theme
exec_always --no-startup-id gsettings set $gnome_iface icon-theme $icon_theme
exec_always --no-startup-id gsettings set $gnome_iface cursor-theme $cursor_theme
exec --no-startup-id feh --no-fehbg --bg-scale $wallpaper_path
exec --no-startup-id dunst
exec --no-startup-id redshift -t 6500:3500 -m randr:screen=0
