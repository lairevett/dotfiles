PROMPT='$(get_prompt)'
PROMPT2=". "
RPROMPT='%{$(echotc UP 1)%} $(git_prompt_status) $(get_return_status)%{$(echotc DO 1)%}'

zstyle ':completion:*:descriptions' format "%B--- %d%b"
typeset -A ZSH_HIGHLIGHT_STYLES
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]="none"
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]="none"
ZSH_HIGHLIGHT_STYLES[path]="fg=white,underline"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[green]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg[red]%}✗%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[green]%}✔%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}§%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}✚%{$reset_color%}"

VIRTUAL_ENV_DISABLE_PROMPT=1

function get_venv_info {
    if [[ ! -z "$VIRTUAL_ENV" ]]; then
      local dir=$(basename "$VIRTUAL_ENV")
      echo "($dir) "
    fi
}

function get_git_repo_info {
    if [ -d .git ]; then
        echo "\\n%{$fg[white]%}$(git remote get-url origin)%{$reset_color%} $(git_prompt_info)"
    fi;
}

function get_prompt {
    local color
    local sign

    if [[ "$USER" == "root" ]]; then
      color="red"
      sign="#"
    else
      color="blue"
      sign="$"
    fi

    echo "$(get_git_repo_info)\\n$(get_venv_info)%{$fg[$color]%}$sign%{$reset_color%} "
}

function get_return_status {
    echo "%(?..%{$fg[red]%}%?!%{$reset_color%})"
}
