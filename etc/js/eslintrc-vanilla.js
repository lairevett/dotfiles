module.exports = {
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'script',
        ecmaFeatures: {
            impliedStrict: true,
        },
    },
    extends: [
        'airbnb-base',
        'eslint:recommended',
        'plugin:node/recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:promise/recommended',
        'plugin:compat/recommended',
        'plugin:jest/recommended',
        'plugin:unicorn/recommended',
        'plugin:security/recommended',
        'plugin:prettier/recommended',
        'prettier',
    ],
    plugins: ['prefer-arrow', 'promise', 'xss', 'jest', 'unicorn', 'security', 'html', 'prettier'],
    env: {
        browser: true,
        node: true,
        es6: true,
        es2020: true,
        jest: true,
    },
    rules: {
        'no-console': 'off',
        'no-underscore-dangle': 'off',
        'no-bitwise': [
            'error',
            {
                allow: ['~'],
            },
        ],
        'prefer-arrow/prefer-arrow-functions': [
            'warn',
            {
                disallowPrototype: true,
                singleReturnOnly: false,
                classPropertiesAllowed: true,
            },
        ],
        'xss/no-mixed-html': 2,
        'xss/no-location-href-assign': 2,
        'unicorn/filename-case': [
            'error',
            {
                case: 'snakeCase',
            },
        ],
        'prettier/prettier': 'warn',
    },
    settings: {
        'import/extensions': ['.js', '.cjs', '.mjs'],
        'html/indent': '+4',
        'html/report-bad-indent': 'warn',
    },
};
