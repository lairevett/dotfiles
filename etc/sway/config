font pango:IBM Plex Mono 10
focus_follows_mouse no
gaps inner 4
gaps outer 8

# Hide cursor after 1s.
seat * hide_cursor 1000

# Variables.
set $mod Mod4
set $alt Mod1

set $mode_system SYS
set $mode_resize RES

set $left h
set $down j
set $up k
set $right l

set $term termite

set $menu 'rofi -modi drun -show drun -theme ~/.config/rofi/themes/lrvtt'
set $lock 'swaylock -c 000000e6 -f -F -l --bs-hl-color ff0000 --caps-lock-bs-hl-color ff0000 \
                    --caps-lock-key-hl-color 33ff00 --indicator-radius 96 \
                    --indicator-thickness 4 --inside-color 000000e6 \
                    --inside-clear-color 000000e6 --inside-caps-lock-color 000000e6 \
                    --inside-ver-color 000000e6 --inside-wrong-color 000000e6 \
                    --key-hl-color 33ff00 --line-color 000000e6 --line-clear-color 000000e6 \
                    --line-caps-lock-color 000000e6 --line-ver-color 000000e6 --line-wrong-color 000000e6 \
                    --ring-color ffffff --ring-clear-color ffffff --ring-caps-lock-color ffffff \
                    --ring-ver-color ffffff --ring-wrong-color ffffff --separator-color ffffff \
                    --text-color ffffff --text-clear-color ffffff --text-caps-lock-color ffffff \
                    --text-ver-color ffffff --text-wrong-color ffffff'

set $wallpaper_path '/usr/share/backgrounds/lrvtt/wallpaper.png'

set $ws1 '1:  Desktop'
set $ws2 '2:  Browser'
set $ws3 '3:  Dev'
set $ws4 '4:  Multimedia'
set $ws5 '5:  Mail'
set $ws6 '6:  IMs'
set $ws7 '7:  VMs'
set $ws8 '8:  Terminal'

# Output configuration.
output * resolution 1920x1080 position 1920,0 bg $wallpaper_path fill

# Idle configuration.
#
# Lock screen after 15 minutes of inactivity,
# turn off screens after another 15 minutes of inactivity,
# turn back off when resumed,
# lock screen before computer goes to sleep.
exec swayidle -w \
        timeout 900 '$lock' \
        timeout 1800 'swaymsg "output * dpms off"' \
        resume 'swaymsg "output * dpms on"' \
        before-sleep '$lock'

# Input configuration.
input * repeat_delay 500
input * repeat_rate 24

input '1739:32382:MSFT0001:00_06CB:7E7E_Touchpad' {
    accel_profile flat
    click_method clickfinger
    drag enabled
    drag_lock disabled
    dwt enabled
    events enabled
    left_handed disabled
    middle_emulation enabled
    natural_scroll enabled
    pointer_accel 0.2
    scroll_button disable
    scroll_factor 1.0
    scroll_method two_finger
    tap enabled
    tap_button_map lrm
}

input '1:1:AT_Translated_Set_2_keyboard' {
    xkb_layout pl,gb
    xkb_model inspiron
    xkb_options grp:alt_shift_toggle,caps:escape
    xkb_rules evdev
    xkb_variant ,
    xkb_capslock disabled
    xkb_numlock disabled
}

# Key bindings.
bindsym $mod+Return exec $term
bindsym $mod+Shift+q kill
bindsym $mod+d exec $menu
floating_modifier $mod normal
bindsym $mod+Shift+c reload

bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8

bindcode $mod+59 workspace prev # mod + comma
bindcode $mod+60 workspace next # mod + full stop

bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8

bindsym $mod+b splith
bindsym $mod+v splitv

bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

bindsym $mod+f fullscreen
bindsym $mod+Shift+space floating toggle

bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

bindsym $mod+Shift+minus move scratchpad
bindsym $mod+minus scratchpad show

mode '$mode_system' {
    bindsym l exec $lock, mode "default"
    bindsym e exec swaymsg exit
    bindsym s exec systemctl suspend
    bindsym h exec systemctl hibernate
    bindsym r exec systemctl reboot
    bindsym p exec systemctl poweroff

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

bindsym $mod+Shift+s mode '$mode_system'

mode '$mode_resize' {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    bindsym Return mode 'default'
    bindsym Escape mode 'default'
}

bindsym $mod+Shift+r mode '$mode_resize'

bindsym XF86AudioMute exec pactl set-sink-mute @DEFAULT_SINK@ toggle
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -5%
bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +5%

bindsym XF86AudioPrev exec playerctl previous
bindsym XF86AudioPlay exec playerctl play-pause
bindsym XF86AudioNext exec playerctl next

bindsym XF86MonBrightnessDown exec brightnessctl set 5%-
bindsym XF86MonBrightnessUp exec brightnessctl set +5%

bindsym --release Print exec grim $(xdg-user-dir PICTURES)/screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')
bindsym --release Control+Print exec grim - | wl-copy
bindsym --release Shift+Print exec grim -g "$(slurp)" $(xdg-user-dir PICTURES)/screenshots/$(date +'%Y-%m-%d-%H%M%S_grim.png')
bindsym --release Control+Shift+Print exec grim -g "$(slurp)" - | wl-copy

# Client colors.
#
# class                 border  backg.  text    indicator child_border
client.focused          #000000 #000000 #ffffff #000000   #000000
client.focused_inactive #000000 #000000 #ffffff #000000   #000000
client.unfocused        #000000 #000000 #808080 #000000   #000000
client.urgent           #000000 #000000 #ff0000 #000000   #000000
client.placeholder      #000000 #000000 #ffffff #000000   #000000
client.background       #000000

# Status bar
bar {
    position top
    status_command i3status --config ~/.config/i3status/top.conf | i3status_injector.py user
    height 32
    gaps 8 12 0 12
    separator_symbol \0
    strip_workspace_numbers no
    binding_mode_indicator no
    tray_output none
    colors {
        background #000000
        statusline #ffffff
        inactive_workspace #000000 #000000 #808080
        active_workspace #000000 #000000 #ffffff
        focused_workspace #000000 #000000 #ffffff
        urgent_workspace #000000 #000000 #ff0000
    }
}

bar {
    position bottom
    status_command i3status --config ~/.config/i3status/bottom.conf | i3status_injector.py merge_cpu_usage_with_temp net_throughput uptime
    gaps 0 12 8 12
    separator_symbol " | "
    tray_output none
    workspace_buttons no
    colors {
        background #000000
        statusline #ffffff
        separator #ffffff
    }
}

# Bind apps to workspaces.
assign [class="Code"] $ws3
assign [class="jetbrains-toolbox"] $ws3
assign [class="jetbrains-webstorm"] $ws3
assign [class="jetbrains-pycharm"] $ws3
assign [class="mpv"] $ws4
assign [class="Spotify"] $ws4
for_window [class="Spotify"] move to workspace $ws4
assign [class="Mailspring"] $ws5
assign [class="whatsapp-nativefier-d52542"] $ws6
assign [class="Skype"] $ws6
assign [class="discord"] $ws6

# Startup commands.
exec_always gsettings set org.gnome.desktop.interface gtk-theme 'oomox-black'
exec_always gsettings set org.gnome.desktop.interface icon-theme 'oomox-black'
exec_always gsettings set org.gnome.desktop.interface cursor-theme 'Adwaita'
exec dunst
exec redshift -t 6500:3500

include /etc/sway/config.d/*
