import atexit
import os
import readline

histfile_path = os.getenv('PYTHONHISTFILE')
histfile = histfile_path if histfile_path else os.path.join(os.path.expanduser("~"), ".python_history")

try:
    readline.read_history_file(histfile)
    # default history len is -1 (infinite), which may grow unruly
    readline.set_history_length(1000)
except FileNotFoundError:
    pass

atexit.register(readline.write_history_file, histfile)
