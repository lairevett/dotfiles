" Jump to the tagbar on open, close it when some tag is selected.
let g:tagbar_autoclose = 1

" Open/close tagbar on leader + b.
nnoremap <Leader>b :TagbarToggle<CR>

" Opened/closed tag arrow color.
highlight TagbarFoldIcon ctermfg=8 guifg=DarkGray
