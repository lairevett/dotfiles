" Show hidden files in NERDTree.
let g:NERDTreeShowHidden=1

" Exit NERDTree after opening a file.
let g:NERDTreeQuitOnOpen=1

" Hide the directory arrows.
let g:NERDTreeDirArrowExpandable=''
let g:NERDTreeDirArrowCollapsible=''

" Prettier git icons.
let g:NERDTreeGitStatusUseNerdFonts=1

" Wider tree buffer.
let g:NERDTreeWinSize=40

" Open/close NERDTree with Leader + t.
nnoremap <Leader>t :NERDTreeToggle<CR>

" Highlight only selected directory/file.
highlight NERDTreeDir ctermfg=8 guifg=DarkGray
highlight NERDTreeDirSlash ctermfg=8 guifg=DarkGray
autocmd BufEnter NERD_tree_* highlight CursorLine ctermfg=15 guifg=White
autocmd BufEnter NERD_tree_* highlight Normal ctermfg=8 guifg=DarkGray
autocmd BufLeave NERD_tree_* highlight CursorLine ctermfg=NONE guifg=NONE
autocmd BufLeave NERD_tree_* highlight Normal ctermfg=15 guifg=White

" Limited nerdtree-syntax-highlight.
let g:NERDTreeLimitedSyntax=1

" Custom syntax highlighting rules.
let g:NERDTreeExactMatchHighlightColor={} " this line is needed to avoid error
let g:NERDTreeExactMatchHighlightColor['.gitignore'] = "808080"
