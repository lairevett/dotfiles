" Set shell to zsh.
set shell=/usr/bin/env\ sh

if !has('nvim')
    " Set terminal size to 10 rows.
    set termwinsize=10x0
endif

" Open terminal in horizontal split below buffer with currently edited file.
nnoremap <Leader>e :below terminal<CR>
