" Don't show default vim statusline.
set noshowmode

" Show current mode, etc. in lightline.
set laststatus=2

" Show tabline.
set showtabline=2

let g:lightline={
    \ 'colorscheme': 'lightline_colorscheme',
    \ 'tabline': {
        \ 'left': [['buffers']],
        \ 'right': [['close']]
    \ },
    \ 'active': {
        \ 'left':[
            \ [ 'mode', 'paste' ],
            \ [ 'gitbranch', 'readonly', 'filename', 'modified', 'cocstatus' ]
        \ ],
        \ 'right': [
            \ [ 'lineinfo' ],
            \ [ 'percent' ],
            \ [ 'filetype' ],
            \ [ 'fileencoding' ],
            \ [ 'fileformat' ],
            \ [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ],
        \ ],
    \ },
    \ 'component': {
        \ 'lineinfo': 'line %3l:%-2v',
    \ },
    \ 'component_function': {
        \ 'gitbranch': 'fugitive#head',
        \ 'cocstatus': 'coc#status',
    \ },
    \ 'component_expand': {
        \ 'buffers': 'lightline#bufferline#buffers',
        \ 'linter_checking': 'lightline#ale#checking',
        \ 'linter_infos': 'lightline#ale#infos',
        \ 'linter_warnings': 'lightline#ale#warnings',
        \ 'linter_errors': 'lightline#ale#errors',
        \ 'linter_ok': 'lightline#ale#ok',
    \ },
    \ 'component_type': {
        \ 'buffers': 'tabsel',
        \ 'linter_checking': 'right',
        \ 'linter_infos': 'right',
        \ 'linter_warnings': 'warning',
        \ 'linter_errors': 'error',
        \ 'linter_ok': 'right',
    \ },
\ }

" Update lightline on buffer write and text change.
autocmd BufWritePost,TextChanged,TextChangedI * call lightline#update()
