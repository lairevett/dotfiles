" Don't jump to the first result automatically.
cnoreabbrev Ack Ack!

" Use Ack! command in current working directory.
nnoremap <Leader>ad :Ack!<Space>

" Use Ack! command in current git repo project root.
nnoremap <Leader>ag :Gcd <bar> Ack!<Space>
