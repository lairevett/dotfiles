" Coc.nvim displayByAle.
let g:ale_disable_lsp = 1

" Disable error highlighting.
let g:ale_set_highlights = 0

" Keep the sign column open.
let g:ale_sign_column_always = 1

" Change the signs to asterisk (warning is yellow, error red).
let g:ale_sign_error = '*'
let g:ale_sign_warning = g:ale_sign_error

" Don't lint on changed text to save battery life.
let g:ale_lint_on_text_changed='never'

" Don't lint when leaving insert mode to save battery life.
let g:ale_lint_on_insert_leave=0

let g:ale_linters={
    \ 'html': ['htmlhint'],
    \ 'xhtml': ['alex'],
    \ 'css': ['stylelint'],
    \ 'less': ['stylelint'],
    \ 'scss': ['stylelint'],
    \ 'javascript': ['eslint'],
    \ 'javascriptreact': ['eslint'],
    \ 'typescript': ['eslint'],
    \ 'typescriptreact': ['eslint'],
    \ 'python': ['pylint'],
    \ 'php': ['langserver'],
    \ 'json': ['jsonlint'],
    \ 'yaml': ['yamllint'],
    \ 'xml': ['xmllint'],
    \ 'sql': ['sqlint'],
    \ 'markdown': ['markdownlint'],
    \ 'sh': ['shellcheck'],
    \ 'dockerfile': ['hadolint'],
    \ 'vim': ['vint'],
\ }

let g:ale_fixers={
    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'html': ['prettier'],
    \ 'css': ['prettier'],
    \ 'less': ['prettier'],
    \ 'scss': ['prettier'],
    \ 'javascript': ['eslint'],
    \ 'javascriptreact': ['eslint'],
    \ 'typescript': ['eslint'],
    \ 'typescriptreact': ['eslint'],
    \ 'python': ['autopep8'],
    \ 'php': ['prettier'],
    \ 'json': ['prettier'],
    \ 'yaml': ['prettier'],
    \ 'xml': ['prettier'],
    \ 'sql': ['sqlformat'],
    \ 'markdown': ['prettier'],
    \ 'sh': ['shfmt'],
\ }

let g:ale_python_pylint_options='--rcfile ' . $PYLINTRC
let g:ale_python_pylint_use_global=0
let g:ale_python_autopep8_options='--global-config ' . $AUTOPEP8RC

" Format code on leader + af.
nnoremap <Leader>af :ALEFix<CR>

" Move between linting errors with ]r and [r.
nnoremap [r :ALEPreviousWrap<CR>
nnoremap ]r :ALENextWrap<CR>

" Color scheme.
highlight ALEErrorSign ctermbg=0 guibg=Black ctermfg=9 guifg=LightRed
highlight ALEWarningSign ctermbg=0 guibg=Black ctermfg=11 guifg=LightYellow
