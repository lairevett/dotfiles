" Enable syntax highlighting.
syntax enable

" Adjust vim to use darker colors.
set background=dark

" Allow color schemes to do bright colors without forcing bold.
if &t_Co == 8 && $TERM !~# '^Eterm'
    set t_Co=16
endif

" Different text color for selected line number.
set cursorline

" Require .jsx extension for jsx syntax to work.
let g:jsx_ext_required=1

" Editor colors.
highlight CursorLine cterm=NONE gui=NONE ctermbg=0 guibg=Black
highlight CursorColumn cterm=NONE gui=NONE ctermbg=0 guibg=Black
highlight Delimiter ctermfg=8 guifg=DarkGray " e.g. markdown '#', json '{}[]'.
highlight VertSplit ctermbg=0 guibg=Black ctermfg=0 guifg=Black
highlight Folded ctermbg=0 guibg=Black
highlight FoldColumn ctermbg=0 guibg=Black
highlight SignColumn ctermbg=0 guibg=Black
highlight LineNr ctermbg=0 guibg=Black ctermfg=8 guifg=DarkGray
highlight CursorLineNr cterm=NONE gui=NONE ctermbg=0 guibg=Black ctermfg=15 guifg=White
highlight MatchParen ctermbg=7 guibg=LightGray ctermfg=0 guifg=Black
highlight NonText ctermbg=0 guibg=Black ctermfg=12 guifg=LightBlue
highlight Normal ctermbg=0 guibg=Black ctermfg=15 guifg=White
highlight Pmenu ctermbg=0 guibg=Black ctermfg=8 guifg=DarkGray
highlight PmenuSel ctermbg=0 guibg=Black ctermfg=15 guifg=White
highlight PmenuSbar ctermbg=0 guibg=Black
highlight PmenuThumb ctermbg=8 guibg=DarkGray
highlight SpellBad cterm=UNDERLINE gui=UNDERLINE ctermbg=0 guibg=Black
highlight SpellCap cterm=UNDERLINE gui=UNDERLINE ctermbg=0 guibg=Black
highlight Title cterm=BOLD gui=BOLD ctermfg=7 guifg=LightGray " e.g. html title, markdown title.
highlight DiffAdd ctermbg=0 guibg=Black ctermfg=10 guifg=LightGreen
highlight DiffChange ctermbg=0 guibg=Black ctermfg=12 guifg=LightBlue
highlight DiffDelete ctermbg=0 guibg=Black ctermfg=9 guifg=LightRed

" Syntax highlighting colors.
highlight Include ctermfg=12 guifg=LightBlue
highlight PreProc ctermfg=7 guifg=LightGray
highlight Comment ctermfg=8 guifg=DarkGray
highlight Todo cterm=BOLD gui=BOLD ctermbg=8 guibg=DarkGray ctermfg=11 guifg=LightYellow

highlight Keyword ctermfg=12 guifg=LightBlue
highlight Identifier cterm=NONE gui=NONE ctermfg=12 guifg=LightBlue " e.g. 'let' in JavaScript.
highlight Type ctermfg=12 guifg=LightBlue
highlight Operator cterm=BOLD gui=BOLD ctermfg=13 guifg=LightMagenta " e.g. 'new' in JavaScript or 'and, not' in Python.
highlight Special ctermfg=15 guifg=White " e.g. inline CSS/JS in HTML files.

highlight Braces ctermfg=13 guifg=LightMagenta
highlight default link Brackets Braces
highlight default link Parens Braces
call matchadd("Braces", "[{}]")
call matchadd("Brackets", "[[\\]]")
call matchadd("Parens", "[()]")

highlight Boolean ctermfg=12 guifg=LightBlue
highlight Character ctermfg=9 guifg=LightRed
highlight Float ctermfg=15 guifg=White
highlight Number ctermfg=15 guifg=White
highlight String ctermfg=10 guifg=LightGreen

highlight Conditional ctermfg=13 guifg=LightMagenta
highlight Function ctermfg=14 guifg=LightCyan
highlight Statement ctermfg=13 guifg=LightMagenta
highlight Label cterm=NONE gui=NONE ctermfg=11 guifg=LightYellow " e.g. 'case' in switch-case statements.
highlight Repeat ctermfg=13 guifg=LightMagenta " e.g. 'for' loop in JavaScript.
highlight Structure ctermfg=12 guifg=LightBlue

highlight Constant ctermfg=11 guifg=LightYellow " e.g. '$' in JavaScript.
highlight Define ctermfg=7 guifg=LightGray " e.g. '@' in Python decorators.
highlight StorageClass ctermfg=9 guifg=LightRed " e.g. css properties.

highlight htmlTag ctermfg=8 guifg=DarkGray
highlight default link htmlEndTag htmlTag
highlight htmlTagName ctermfg=12 guifg=LightBlue
highlight default link htmlTagN htmlTagName
highlight htmlArg ctermfg=15 guifg=White

highlight cssIdentifier ctermfg=7 guifg=LightGray
highlight cssClassName ctermfg=7 guifg=LightGray
highlight default link cssClassNameDot cssClassName
highlight cssProp ctermfg=13 guifg=LightMagenta
highlight cssAttr ctermfg=15 guifg=White
highlight default link cssAttr cssCommonAttr
highlight cssColor ctermfg=15 guifg=White

autocmd! Syntax javascript,javascriptreact :syntax keyword javaScriptInclude import from |
            \ syntax match javaScriptDefinedOrCalledFunctionName /[0-9a-zA-Z_]*\ze(/ |
            \ syntax match javaScriptArrowFunctionName /\v(\(.*)@<![0-9a-zA-Z_]+\ze(\s+)?\=(\s+)?.*\=\>/ |
            \ syntax match javaScriptFatArrow /=>/ |
            \ syntax match javaScriptOperator /===\|==\|!==\|!=\|<=\||>=\|!\|&\||\|\^\|\~/
            "\ syntax match javaScriptGreaterLessThanOperator /\s\zs>|<\ze\s/ TODO: distinction between jsx tags

highlight default link javaScriptInclude Include
highlight default link javaScriptDefinedOrCalledFunctionName Function
highlight default link javaScriptArrowFunctionName Function
highlight default link javaScriptFunction Keyword " Only the 'function' keyword.
highlight default link javaScriptOperator Operator
highlight default link javaScriptGreaterLessThanOperator javaScriptOperator
highlight javaScriptFatArrow cterm=BOLD gui=BOLD ctermfg=12 guifg=LightBlue
" highlight default link javaScriptParens Parens
highlight javaScriptEmbed ctermfg=7 guifg=LightGray
highlight javaScriptEmbedBraces ctermfg=11 guifg=LightYellow
highlight default link jsxTag htmlTag
highlight default link jsxEndTag htmlEndTag
highlight default link jsxTagName htmlTagName
highlight default link jsxComponentName jsxTagName
highlight default link jsxAttrib htmlAttrib
" highlight default link jsxBraces javaScriptBraces
highlight default link jsxEqual htmlTag

" highlight jsonBraces ctermfg=12 guifg=LightBlue
highlight jsonKeyword ctermfg=13 guifg=LightMagenta

autocmd! Syntax python :syntax keyword pythonKeyword class def nextgroup=pythonFunction skipwhite |
            \ syntax match pythonFunctionCall /\w*\ze(/ |
            \ syntax keyword pythonKeyword self |
            \ syntax keyword pythonAwait await |
            \ syntax keyword pythonBoolean True False |
            \ syntax keyword pythonNone None |
            \ syntax match pythonOperator /==\|!=\|<=\|<\|>=\|>\|&\||\|\^\|\~/ |
            \ syntax match pythonKwargName /\v[\(\,]\s{-}\zs\w+\ze\s{-}\=(\=)@!/ |
            \ syntax match pythonAsteriskArgsKwargs /\v*{1,2}[a-zA-Z_]+/ contains=pythonKeyword

highlight default link pythonInclude Include
highlight default link pythonFunction Function
highlight default link pythonFunctionCall Function
highlight default link pythonBuiltin Function
highlight default link pythonKeyword Keyword
highlight default link pythonAsync pythonKeyword
highlight default link pythonAwait pythonStatement
highlight pythonExceptions ctermfg=7 guifg=LightGray
highlight default link pythonBoolean Boolean
highlight default link pythonNone pythonBoolean
highlight default link pythonOperator Operator
highlight pythonKwargName ctermfg=7 guifg=LightGray
highlight pythonAsteriskArgsKwargs ctermfg=7 guifg=LightGray

" To find out.
highlight SpecialChar cterm=BOLD gui=BOLD ctermfg=14
highlight Tag cterm=BOLD gui=BOLD ctermfg=14
highlight Typedef cterm=BOLD gui=BOLD ctermfg=14
