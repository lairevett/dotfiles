KEEP_POINTER = True
BATTERY_REMAINING_EMPTY = ', h'
BATTERY_INDEX = 4


def main(bar_modules):
    bar_module = bar_modules[BATTERY_INDEX]
    is_battery_remaining_empty = bar_module['full_text'].find(BATTERY_REMAINING_EMPTY) > -1
    if is_battery_remaining_empty:
        bar_module['full_text'] = bar_module['full_text'].replace(BATTERY_REMAINING_EMPTY, '')
