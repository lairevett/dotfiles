import subprocess


KEEP_POINTER = True
WLAN_MAX_RSSI = 63.5
WLAN_QUALITY_AND_FREQUENCY_EMPTY = '?, ?'
WLAN_INDEX = 2


def get_wlan_channel_and_rssi():
    process = subprocess.run(
        'ifconfig wlan0 list sta | sed -n 2p | cut -w -f3,5',
        shell=True,
        capture_output=True
    )

    stdout = process.stdout.decode('utf-8')
    success = bool(stdout)
    return (*(stdout.split() if success else (None, None,)), success,)


def parse_rssi(rssi):
    return f'{round(float(rssi) / WLAN_MAX_RSSI * 100)}%'


def parse_frequency(frequency):
    return f'{str(int(frequency) / 1000).replace(".", ",")} GHz'


def get_wlan_freqency_func():
    last_channel = None
    last_frequency = None

    def closure(channel):
        nonlocal last_channel
        nonlocal last_frequency

        if channel == last_channel:
            return last_frequency

        process = subprocess.run(
            f'ifconfig wlan0 list chan | grep -o -Ee "Channel\s+{channel} : [0-9]+" | cut -d: -f2',
            shell=True,
            capture_output=True
        )

        last_channel = channel
        frequency = parse_frequency(process.stdout.decode("utf-8"))
        last_frequency = frequency

        return frequency

    return closure


get_wlan_freqency = get_wlan_freqency_func()


def main(bar_modules):
    bar_module = bar_modules[WLAN_INDEX]
    is_wlan_quality_and_frequency_empty = bar_module['full_text'].find(WLAN_QUALITY_AND_FREQUENCY_EMPTY)
    if is_wlan_quality_and_frequency_empty:
        channel, rssi, success = get_wlan_channel_and_rssi()
        if success:
            quality = parse_rssi(rssi)
            frequency = get_wlan_freqency(channel)
            bar_module['full_text'] = bar_module['full_text'].replace(
                WLAN_QUALITY_AND_FREQUENCY_EMPTY,
                f'{quality}, {frequency}'
            )
