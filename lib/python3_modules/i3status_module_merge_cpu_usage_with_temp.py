KEEP_POINTER = True
CPU_USAGE_INDEX = 1
CPU_TEMP_INDEX = 2


def main(bar_modules):
    cpu_temp = bar_modules[CPU_TEMP_INDEX]['full_text'].replace('.', ',')
    del bar_modules[CPU_TEMP_INDEX]
    cpu_usage_module = bar_modules[CPU_USAGE_INDEX]
    cpu_usage_module['full_text'] += cpu_temp
