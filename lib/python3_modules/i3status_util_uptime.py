from datetime import timedelta


def get_uptime_and_unit(seconds):
    uptime = str(timedelta(seconds=seconds)).split(':')
    unit = 'hr' if uptime[0] == 1 else 'hrs'

    return uptime, unit


def get_i3bar_data(cb_get_elapsed_seconds):
    seconds = cb_get_elapsed_seconds()
    uptime, unit = get_uptime_and_unit(seconds)

    return {
        'position': -1,
        'data': {
            'name': 'uptime',
            'full_text': f'UP: {uptime[0]} {unit}, {uptime[1]} min',
            'separator': ' '
        }
    }
