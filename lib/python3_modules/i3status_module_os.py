import os


KEEP_POINTER = False


def main():
    uname = os.uname()

    return {
        'position': 0,
        'data': {
            'name': 'os',
            'full_text': f'OS: {uname.sysname} {uname.release}'
        }
    }
