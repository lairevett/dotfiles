import os
import pwd


KEEP_POINTER = False


def main():
    passwd_entry = pwd.getpwuid(os.getuid())
    full_name = passwd_entry.pw_gecos
    username = passwd_entry.pw_name
    hostname = os.uname().nodename.split('.')[0]

    return {
        'position': -2,  # Display user info before date and time module.
        'data': {
            'name': 'user',
            'full_text': f'  {full_name} ({username}@{hostname})',
            'separator': True
        }
    }
