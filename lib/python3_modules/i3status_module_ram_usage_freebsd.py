from freebsd_sysctl import Sysctl


KEEP_POINTER = True


def get_page_size():
    page_size = None

    def closure():
        nonlocal page_size

        if page_size is None:
            page_size = Sysctl('hw.pagesize').value

        return page_size

    return closure()


def get_free_ram_blocks_count():
    return Sysctl('vm.stats.vm.v_free_count').value


def get_total_ram():
    total_ram = None

    def closure():
        nonlocal total_ram

        if total_ram is None:
            total_ram = Sysctl('hw.physmem').value

        return total_ram

    return closure()


def parse_to_gb(ram):
    rounded = round(ram / 1024000000, 1)
    return f'{str(rounded).replace(".", ",")} GB'


def main(_):
    free_ram = get_free_ram_blocks_count() * get_page_size()
    total_ram = get_total_ram()
    used_ram = total_ram - free_ram
    used_ram_percentage = round(used_ram / total_ram * 100)

    return {
        'position': 2,
        'data': {
            'name': 'ram_usage',
            'full_text': f'RAM: {parse_to_gb(used_ram)} ({used_ram_percentage}%)',
        }
    }
