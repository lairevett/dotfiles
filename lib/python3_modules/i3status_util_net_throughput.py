import time


def get_rate_with_unit(delta_bytes):
    """ Converts bytes/s to {X}bits/s and returns it with unit. """
    bits = delta_bytes * 8

    if bits < 1024:
        rate, unit = bits, ' bps'
    elif bits < 1024000:
        rate, unit = bits / 1024, 'kbps'
    elif bits < 1024000000:
        rate, unit = bits / 1024000, 'Mbps'
    else:
        rate, unit = bits / 1024000000, 'Gbps'

    return f'{round(rate):03} {unit}'


def get_tx_and_rx_rate(cb_get_tx_and_rx_bytes):
    prev_tx_bytes, prev_rx_bytes = cb_get_tx_and_rx_bytes()

    time.sleep(1)

    curr_tx_bytes, curr_rx_bytes = cb_get_tx_and_rx_bytes()
    delta_tx_bytes, delta_rx_bytes = curr_tx_bytes - prev_tx_bytes, curr_rx_bytes - prev_rx_bytes

    tx_rate = get_rate_with_unit(delta_tx_bytes)
    rx_rate = get_rate_with_unit(delta_rx_bytes)
    return tx_rate, rx_rate


def get_i3bar_data(tx_rate, rx_rate):
    return {
        'position': -1,
        'data': {
            'name': 'net_throughput',
            'full_text': f'NET: {tx_rate} TX, {rx_rate} RX'
        }
    }
