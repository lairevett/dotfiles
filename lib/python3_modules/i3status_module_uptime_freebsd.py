import subprocess
from time import time
from i3status_util_uptime import get_i3bar_data


KEEP_POINTER = True


def get_kernel_boot_time():
    kernel_boot_time = None

    def closure():
        nonlocal kernel_boot_time

        if kernel_boot_time is None:
            process = subprocess.run('sysctl -n kern.boottime | cut -d= -f2 | cut -d, -f1',
                                     shell=True, capture_output=True)
            kernel_boot_time = int(process.stdout.decode('utf-8'))

        return kernel_boot_time

    return closure()


def get_elapsed_seconds():
    seconds_since_unix_epoch = time()
    kernel_boot_time = get_kernel_boot_time()
    return seconds_since_unix_epoch - kernel_boot_time


def main(_):
    return get_i3bar_data(get_elapsed_seconds)
