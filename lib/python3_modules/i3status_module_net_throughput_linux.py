from i3status_util_net_throughput import get_tx_and_rx_rate, get_i3bar_data


KEEP_POINTER = True


def get_netstats_filename(direction):
    return f'/sys/class/net/wlp1s0/statistics/{direction}_bytes'


def get_tx_and_rx_bytes():
    """ Opens file with wlp1s0 interface transmitted/received bytes. """
    with open(get_netstats_filename('tx'), 'r') as tx_file, open(get_netstats_filename('rx'), 'r') as rx_file:
        tx_bytes = int(tx_file.read())
        rx_bytes = int(rx_file.read())

    return tx_bytes, rx_bytes


def main(_):
    tx_rate, rx_rate = get_tx_and_rx_rate(get_tx_and_rx_bytes)
    return get_i3bar_data(tx_rate, rx_rate)
