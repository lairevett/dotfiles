import subprocess
from i3status_util_net_throughput import get_tx_and_rx_rate, get_i3bar_data


KEEP_POINTER = True


def get_interface_stats():
    """ Uses netstat to get interface stats. """
    # Ignore failover lagg interfaces.
    process = subprocess.run(
        'netstat -i -b | grep -v "lagg" | grep -Ee "<Link#[0-9]{1,}>" | tr -s "[:blank:]"', shell=True, capture_output=True)
    return process.stdout.decode('utf-8')


def get_tx_and_rx_bytes():
    """ Gets Ibytes/Obytes column from interface stats. """
    rx_bytes = 0
    tx_bytes = 0
    all_stats = get_interface_stats().split('\n')[:-1]

    for line in all_stats:
        stats = line.split()

        # Name, Mtu, Network, Address, Ipkts, Ierrs, Idrop, Ibytes, Opkts, Oerrs, Obytes, Coll
        # RX == Ibytes (8th), TX == Obytes (11th)
        rx_bytes += int(stats[7])
        tx_bytes += int(stats[10])

    return tx_bytes, rx_bytes


def main(_):
    tx_rate, rx_rate = get_tx_and_rx_rate(get_tx_and_rx_bytes)
    return get_i3bar_data(tx_rate, rx_rate)
