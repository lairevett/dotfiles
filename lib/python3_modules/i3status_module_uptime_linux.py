from datetime import timedelta


KEEP_POINTER = True


def get_elapsed_seconds():
    with open('/proc/uptime') as file:
        return float(file.read().split()[0])


def main(bar_modules):
    seconds = get_elapsed_seconds()
    uptime = str(timedelta(seconds=seconds)).split(':')
    unit = 'hr' if uptime[0] == 1 else 'hrs'

    return {
        'position': len(bar_modules),
        'data': {
            'name': 'uptime',
            'full_text': f'UP: {uptime[0]} {unit}, {uptime[1]} min '
        }
    }
