#!/bin/sh

CODE="$1"

case "$CODE" in
    "0x11")
        /usr/local/bin/acpi_thinkpad_brightness.sh down 10
        ;;
    "0x10")
        /usr/local/bin/acpi_thinkpad_brightness.sh up 10
        ;;
    "0x1b")
        /usr/local/bin/acpi_thinkpad_mic_toggle.sh
        ;;
esac;
