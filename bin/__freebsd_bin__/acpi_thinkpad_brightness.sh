#!/bin/sh

BRIGHTNESS_KEY="hw.acpi.video.lcd0.brightness"
MIN_LEVEL=0
MAX_LEVEL=100
CURRENT_LEVEL="$(/sbin/sysctl -n $BRIGHTNESS_KEY)"
DIRECTION="$1"
STEP="$2" # -1/+1 (acpi_video changing the brightness on its own)

if [ "$DIRECTION" == "up" ]; then
    NEXT_LEVEL="$(($CURRENT_LEVEL + $STEP - 1))"

    if [ "$NEXT_LEVEL" -gt "$MAX_LEVEL" ]; then
        NEXT_LEVEL="$MAX_LEVEL"
    fi;
elif [ "$DIRECTION" == "down" ]; then
    NEXT_LEVEL="$(($CURRENT_LEVEL - $STEP + 1))"

    if [ "$NEXT_LEVEL" -lt "$MIN_LEVEL" ]; then
        NEXT_LEVEL="$MIN_LEVEL"
    fi;
fi;

/sbin/sysctl $BRIGHTNESS_KEY=$NEXT_LEVEL
