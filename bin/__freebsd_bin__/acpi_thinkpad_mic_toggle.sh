#!/bin/sh

MIC_LED_KEY="dev.acpi_ibm.0.mic_led"
CURRENT_VALUE="$(/usr/sbin/mixer rec | /usr/bin/cut -w -f 7)"
MUTED_VALUE="0:0"
UNMUTED_VALUE="53:53"

if [ "$CURRENT_VALUE" = "$MUTED_VALUE" ]; then
    /usr/sbin/mixer rec $UNMUTED_VALUE
    MIC_LED_ON=0
else
    /usr/sbin/mixer rec $MUTED_VALUE
    MIC_LED_ON=1
fi;

/sbin/sysctl $MIC_LED_KEY=$MIC_LED_ON
