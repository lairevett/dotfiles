#!/usr/bin/env /usr/home/msadowski/share/python/pyenv/shims/python
# -*- coding: utf-8 -*-

import sys
import json
import importlib


def load_outside_modules():
    """ Imports i3status modules by names passed to argv. """
    outside_modules = []

    if len(sys.argv) > 1:
        for module_name in sys.argv[1:]:
            try:
                module = importlib.import_module(
                    f'i3status_module_{module_name}')
                outside_modules.append(
                    module.main if module.KEEP_POINTER else module.main())
            except ModuleNotFoundError:
                print(f'Module {module_name} not found!')
                sys.exit(100)

    return outside_modules


def inject_outside_modules(outside_modules, bar_modules):
    """ Modifies bar_modules by inserting loaded modules' JSON into it. """
    for module in outside_modules:
        try:
            # Some modules, like "user" are static, there is no need to keep pointer to them.
            evaluated_module = module(
                bar_modules) if callable(module) else module
            if isinstance(evaluated_module, dict):
                # Position == -1 = last, -2 = second to last, etc.
                position = evaluated_module['position']
                actual_position = position if position >= 0 else len(bar_modules) + position + 1
                bar_modules.insert(actual_position, evaluated_module['data'])
        except json.JSONDecodeError:
            print(f'Module {module} did not return JSON to stdout!')
            sys.exit(100)


def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()


def read_line():
    """ Interrupted respecting reader for stdin. """
    # Try reading a line, removing any extra whitespace.
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line.
        if not line:
            sys.exit(3)
        return line
    # Exit on ctrl-c.
    except KeyboardInterrupt:
        sys.exit()


def get_prefix_and_json_bar_modules(bar_modules):
    """
    All of array elements aside from the first one start with a comma.
    Split and return tuple containing the prefix and bar modules, so JSON parser doesn't throw.
    """
    if bar_modules.startswith(','):
        return (',', bar_modules.split(',', maxsplit=1)[1])

    return ('', bar_modules)


if __name__ == '__main__':
    OUTSIDE_MODULES = load_outside_modules()

    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains beggining of the infinite array.
    print_line(read_line())

    while True:
        prefix, json_bar_modules = get_prefix_and_json_bar_modules(read_line())
        bar_modules = json.loads(json_bar_modules)
        inject_outside_modules(OUTSIDE_MODULES, bar_modules)
        print_line(prefix + json.dumps(bar_modules))
