#!/bin/sh

if [ -n "$BW_SESSION" ]; then
    ssh_agent_pidfile="$SSH_RUNTIME_DIR/agent.pid"
    ssh_agent_pid="$([ -f $ssh_agent_pidfile ] && cat $ssh_agent_pidfile || echo 1)"
    ssh_agent_listening=`(lsof -Di -aUp $ssh_agent_pid | grep ssh-agent) >/dev/null 2>&1 && echo 1 || echo 0`

    if [ "$ssh_agent_listening" = "0" ]; then
        [ -S "$SSH_AUTH_SOCK" ] && rm -f "$SSH_AUTH_SOCK"

        echo "Starting new ssh-agent process."
        eval "$(ssh-agent -s -a $SSH_AUTH_SOCK)"
        echo "$SSH_AGENT_PID" >"$ssh_agent_pidfile"

        echo "Syncing bitwarden vault."
        bw sync

        echo "Getting keyring folder id."
        folder_id="$(bw list folders --search Keyring | \
            cut -d, -f2 | \
            cut -d: -f2 | \
            sed -e 's/^"//' -e 's/"$//'
        )"

        for key in $(echo $SSH_ACTIVE_KEYS_LIST); do
            echo "Trying to add $key to the ssh-agent."
            key_path="$SSH_KEYS_HOME/$key"
            # If ssh-add is connected to any console and doesn't have $DISPLAY, $SSH_ASKPASS is ignored.
            # echo 1 and pipe disconnects ssh-add from the TTY.
            BW_FOLDER_ID="$folder_id" BW_SSH_KEY_NAME="$key" timeout 5s /bin/sh -c "echo 1 | \
                SSH_ASKPASS=bw_get_password.sh DISPLAY= ssh-add $key_path"
            [ "$?" != "0" ] && echo "Failed to add $key_path to the ssh-agent."
            unset key_path
        done

        ((pwait "$SSH_AGENT_PID" && rm -f "$SSH_RUNTIME_DIR/"*) &) >/dev/null
        unset SSH_AGENT_PID
        unset folder_id
    else
        echo "Using already running ssh-agent pid $ssh_agent_pid."
        echo "$ssh_agent_pid" >"$ssh_agent_pidfile"
    fi

    unset ssh_agent_pidfile
    unset ssh_agent_pid
    unset ssh_agent_listening
else
    echo "Failed to connect to account (BW_SESSION not provided)."
fi
