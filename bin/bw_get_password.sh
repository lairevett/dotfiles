#!/bin/sh

if [ ! -n "$BW_FOLDER_ID" ]; then
    BW_FOLDER_ID="$(bw list folders --search Keyring | \
        cut -d, -f2 | \
        cut -d: -f2 | \
	sed -e 's/^"//' -e 's/"$//'
    )"
fi;

if [ ! -n "$BW_SSH_KEY_NAME" ]; then
    BW_SSH_KEY_NAME="$1"
fi;

if [ -n "$BW_FOLDER_ID" ] || [ -n "$BW_SSH_KEY_NAME" ]; then
    item="$(bw list items --folderid $BW_FOLDER_ID --search $BW_SSH_KEY_NAME)"
    [ "$item" = "[]" ] && exit 1

    password="$(echo $item | \
        grep -o '{"name":"password","value":".*","type":0}' | \
        cut -d, -f2 | \
        cut -d: -f2 | \
        sed -e 's/^"//' -e 's/"$//'
    )"
    [ -z "$password" ] && exit 1

    echo "$password"
    exit 0
fi

exit 1
