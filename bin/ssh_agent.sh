#!/bin/sh
if [ ! -d "$SSH_RUNTIME_DIR" ]; then
        mkdir "$SSH_RUNTIME_DIR"
fi;

ssh_agent_pidfile="$SSH_RUNTIME_DIR/agent.pid"
ssh_agent_pid="$([ -f $ssh_agent_pidfile ] && cat $ssh_agent_pidfile || echo 1)"
ssh_agent_listening=`(lsof -Di -aUp $ssh_agent_pid | grep ssh-agent) >/dev/null 2>&1 && echo 1 || echo 0`

if [ "$ssh_agent_listening" = "0" ]; then
    [ -S "$SSH_AUTH_SOCK" ] && rm -f "$SSH_AUTH_SOCK"

    echo "Starting new ssh-agent process."
    eval "$(ssh-agent -s -a $SSH_AUTH_SOCK)"
    echo "$SSH_AGENT_PID" >"$ssh_agent_pidfile"

    ((pwait "$SSH_AGENT_PID" && rm -f "$SSH_RUNTIME_DIR/"*) &) >/dev/null
    unset SSH_AGENT_PID
else
    echo "Using already running ssh-agent pid $ssh_agent_pid."
    echo "$ssh_agent_pid" >"$ssh_agent_pidfile"
fi

unset ssh_agent_pidfile
unset ssh_agent_pid
unset ssh_agent_listening
