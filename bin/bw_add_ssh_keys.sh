#!/bin/sh

if [ -n "$BW_SESSION" ]; then
    echo "Syncing bitwarden vault."
    bw sync

    echo "Getting keyring folder id."
    folder_id="$(bw list folders --search Keyring | \
        cut -d, -f2 | \
        cut -d: -f2 | \
        sed -e 's/^"//' -e 's/"$//'
    )"

    for arg; do
        echo "Trying to add $arg to the ssh-agent."
        key_path="$SSH_KEYS_HOME/$arg"
        # If ssh-add is connected to any console and doesn't have $DISPLAY, $SSH_ASKPASS is ignored.
        # echo 1 and pipe disconnects ssh-add from the TTY.
        BW_FOLDER_ID="$folder_id" BW_SSH_KEY_NAME="$arg" timeout 5s /bin/sh -c "echo 1 | \
	    SSH_ASKPASS=bw_get_password.sh DISPLAY= ssh-add $key_path"
        [ "$?" != "0" ] && echo "Failed to add $key_path to the ssh-agent."
        unset key_path
    done
else
    echo "Failed to connect to account (BW_SESSION not provided)."
fi
